/*
user (object)

properties > username, email, password
method > login, logout, delete account
*/

// let grades = [85, 95, 100];

// /*
// Structure of an Object, uses {}
// */

// let phone = {
// 	color : "red",
// 	weight : "10 g",
// 	model : "S7"
// }

// let grades = {
// 	quiz1 : 85,
// 	quiz2 : 95,
// 	quiz3 : 100
// }

// Object can contain another object or array or function

// let user = {
// 	firstName : "Juan",
// 	lastName : "dela Cruz",
// 	address : {
// 				houseNum : "15",
// 				street : "Sapphire St.",
// 				city : "Fremont",
// 				country : "USA"
// 	},
// 	email : ["main@email.com", "second@email.com"],
// 	fullName : function() {
// 		return this.firstName + " " + this.lastName;
// 	}
// }

// console.log(user);



// let phone = Object();

// let mobilephone = {};

// // Adding properties
// mobilephone.color = "blue";
// mobilephone.weight = "155 g";
// mobilephone.model = "S7";
// mobilephone.ring = function() {
// 	console.log("Phone is ringing");
// };

// console.log(mobilephone);

// let phone = {
// 	color : "red",
// 	weight : "140 g",
// 	model : "iPhone XS",
// 	ring : function() {
// 		console.log("Phone is ringing");
// 	}
// };

// console.log(phone);

// // accessing object properties using dot notation
// console.log(phone.color);
// console.log(mobilephone.model);

// // accessing object properties using square bracket
// console.log(phone["model"]);


// // re-assign values
// phone.color = "black";
// phone.ring = function() {
// 	console.log("Your phone is ringing!");
// };

// console.log(phone);
// phone.ring();

// delete phone.color;
// console.log(phone);

// Simple Constructor
// let pokemon = {
// 	name : "Pikachu",
// 	attack : "lightning",
// 	health : 100
// }

// Object constructor is a function that initializes an object
function pokemon(name, element) {
	this.name = name;
	this.health = 100;
	this.element = element;
	this.attack = function(target) {
		console.log(`${this.name} attacked ${target.name} with ${this.element}`);
		target.health -= 10;
		console.log(`${target.name}'s health is now at ${target.health}`);
	}
}

let blastoise = new pokemon("Blastoise", "Water Cannon");
let geodude = new pokemon("Geodude", "Solid Right");

blastoise.attack(geodude);
geodude.attack(blastoise);